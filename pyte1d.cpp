#include <iostream>
#include <vector>
#include <random>

#include <vexcl/vexcl.hpp>
#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>

#include "te_system.hpp"

namespace py = pybind11;

//---------------------------------------------------------------------------
vex::Context& ctx(std::string name = "") {
    static vex::Context c(vex::Filter::Env && vex::Filter::Name(name));
    return c;
}

//---------------------------------------------------------------------------
struct thermoelectric {
    state_type s;
    te_system  ode_sys;

    thermoelectric(int n, double L, double I, double K, double Edc, double eta, double T)
        : s(ctx(), n), ode_sys(ctx(), n, L, I, K, Edc, eta, T) {}

    void advance(py::array_t<double> x, py::array_t<double> p, int steps, double dt) {
        static std::mt19937 gen;
        static std::uniform_int_distribution<size_t> seed;

        vex::copy(x.data(), x.data() + ode_sys.n, s(0).begin());
        vex::copy(p.data(), p.data() + ode_sys.n, s(1).begin());

        Stepper stepper;

        for(int i = 0; i < steps; ++i) {
            stepper.do_step(std::ref(ode_sys), s, 0.0, dt);
            ode_sys.fluctuate(s, dt, seed(gen));
        }

        vex::copy(s(0).begin(), s(0).end(), x.mutable_data());
        vex::copy(s(1).begin(), s(1).end(), p.mutable_data());
    }
};

//---------------------------------------------------------------------------
PYBIND11_MODULE(pyte1d, m) {
    m.def("context", [](std::string name) {
            std::ostringstream s; s << ctx(name); py::print(s.str());
            }, py::arg("name") = std::string(""));

    py::class_<thermoelectric>(m, "thermoelectric")
        .def(py::init<int, double, double, double, double, double, double>(),
                    py::arg("n"),
                    py::arg("L"),
                    py::arg("I") = 0.5,
                    py::arg("K") = 0.0462,
                    py::arg("Edc") = 4e-4,
                    py::arg("eta") = 0.02,
                    py::arg("T") = 0.11
                    )
        .def("advance", &thermoelectric::advance,
                py::arg("x"),
                py::arg("p"),
                py::arg("steps") = 1,
                py::arg("dt") = 0.02
                )
        ;
}
