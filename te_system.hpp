#ifndef TE_SYSTEM_HPP
#define TE_SYSTEM_HPP

#include <cmath>

#include <vexcl/vexcl.hpp>
#include <boost/numeric/odeint.hpp>
#include <boost/numeric/odeint/external/vexcl/vexcl.hpp>

namespace odeint = boost::numeric::odeint;

//---------------------------------------------------------------------------
typedef vex::multivector<double, 2> state_type; // s(0) -> x, s(1) -> p

typedef odeint::runge_kutta4_classic<
    state_type, double, state_type, double,
    odeint::vector_space_algebra, odeint::default_operations
    > Stepper;

//---------------------------------------------------------------------------
struct te_system {
    const int    n;
    const double L;
    const double I;
    const double K;
    const double Edc;
    const double eta;
    const double g;

    vex::RandomNormal<double> rnd;
    vex::Reductor<double> sum;
    vex::Reductor<int> cnt;

    te_system(const vex::Context &ctx, int n, double L, double I, double K, double Edc, double eta, double T)
        : n(n), L(L), I(I), K(K), Edc(Edc), eta(eta), g(sqrt(2 * eta * T)), sum(ctx)
    {}

    void operator()(const state_type &s, state_type &dsdt, double t) const {
        auto i = vex::tag<1>(vex::element_index());
        auto p = vex::tag<3>(s(1));

        VEX_FUNCTION(double, dpdt, (int, i)(int, n)(double, L)(double, I)(double, K)(double, Edc)(double, eta)(double, p)(double*, x),
            int il = i - 1;
            int ir = i + 1;
            double xl, xr;

            if (il < 0) {
                il = il + n;
                xl = x[il] - L;
            } else {
                xl = x[il];
            }

            if (ir >= n) {
                ir = il - n;
                xr = x[ir] + L;
            } else {
                xr = x[ir];
            }

            double dxl = x[i] - xl;
            double dxr = x[i] - xr;

            return -K * sin(0.2*x[i]) - eta * p + Edc
                + (dxl < 0 ? I : -I) / (dxl * dxl)
                + (dxr < 0 ? I : -I) / (dxr * dxr)
                ;
        );

        dsdt = std::make_tuple(p, dpdt(i, n, L, I, K, Edc, eta, p, raw_pointer(s(0))));
    }

    void fix_boundaries(state_type &s) const {
        VEX_FUNCTION(double, fix, (double, x)(double, L),
            if (x < 0) x += L;
            if (x > L) x -= L;
        );
        s(0) = fix(s(0), L);
    }

    void fluctuate(state_type &s, double dt, size_t seed) {
        auto i = vex::tag<1>(vex::element_index());
        s(1) += g * sqrt(dt) * rnd(i, seed);
    }
    
    int counter(state_type &s) {
        return cnt(s(0) > L);
    }

    double hamiltonian(const state_type &s) const {
        auto i = vex::tag<1>(vex::element_index());
        auto p = vex::tag<3>(s(1));

        VEX_FUNCTION(double, H, (int, i)(int, n)(double, K)(double, I)(double, p)(double*, x),
            double s = 0.5 * p * p + K * cos(x[i]);
            if (i > 0)   s += I / fabs(x[i] - x[i-1]);
            if (i < n-1) s += I / fabs(x[i] - x[i+1]);
            return s;
        );

        return sum(H(i, n, K, I, p, raw_pointer(s(0))));
    }

};

#endif
